import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


def test_alias_sript_exist(host):
    assert host.file("/etc/profile.d/petenv_alias.sh").exists


def test_petenv_repository_exist(host):
    assert host.file("/opt/petenv/.git").exists


def test_petenv_installed(host):
    pip_packets = host.pip_package.get_packages(
        pip_path="/opt/conda/envs/petenv/bin/pip"
    )
    assert "petenv" in pip_packets


def test_petenv_gui_command(host):
    cmd = host.run("/opt/conda/envs/petenv/bin/petenv-gui -h")
    assert cmd.succeeded is True
