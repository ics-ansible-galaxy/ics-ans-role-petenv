# ics-ans-role-petenv

Ansible role to install petenv.

## Role Variables

```yaml
petenv_git_version: 1.0.3
petenv_git_src: https://gitlab.esss.lu.se/icshwi/petenv.git
petenv_git_dest: /opt/petenv
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-petenv
```

## License

BSD 2-clause
