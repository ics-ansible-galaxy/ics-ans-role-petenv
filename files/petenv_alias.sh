alias petenv="conda activate petenv"
alias petenv_deactivate="conda deactivate"
alias petenv-gui="/opt/conda/envs/petenv/bin/petenv-gui"
